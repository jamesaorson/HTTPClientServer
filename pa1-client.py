#Author: James Osborne
#Course: EGCP-4310
#Assignment: PA1
#Purpose: Provide an http client to connect to the basic web server
#set up for PA1

from socket import *
from sys import argv

def makegetrequeststring():
    return 'Get /' + argv[3] + ' HTTP/1.1'

#Open socket
client = socket(AF_INET, SOCK_STREAM)

#Check if necessary args are provided
if len(argv) >= 4:
    try:
        #Verify provided IP address
        inet_aton(argv[1])
    except error:
        print('Invalid  IP -', argv[1])

    try:
        #Use command line args to connect to server
        client.connect((argv[1], int(argv[2])))
        client.settimeout(2)

        #Grab user input to send to server
        message = makegetrequeststring()
        client.send(message.encode())

        #Receive up to 1024 bytes from the server and decode into a string
        #This should simply be the header response and shouldn't be too large
        server_response = client.recv(1024).decode()
        print(server_response)

        #Check for 200 OK response
        if server_response.split()[1] == '200':
            request_contents = ''
            #Receive data until request returns nothing or timeout hits
            while True:
                data = client.recv(1).decode()
                if not data:
                    break;
                request_contents += data

            print(request_contents)

        client.close()
    except ValueError:
        print('Badly formatted port -', argv[2])
    except ConnectionRefusedError:
        print('Requested IP:port not available -', argv[1] + ':' + argv[2])
    except timeout:
        print('Connection timed out')
        client.close()
else:
    print('Usage: [executable] [server ip] [server port] [filename]')
